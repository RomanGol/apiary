#include <stdio.h>

unsigned long long Table[0x10000];

size_t Len = 8;


int main( int argc, char *argv[] )
{
	static unsigned char keyStream[80];
	FILE * fp = fopen("test.bin", "rb");
	
	fread( &Table, sizeof(Table), 1, fp );
	
	unsigned int next = 0;
	for ( size_t i = 0; i < Len; ++i )
	{
		memcpy( keyStream + i * 8, Table + next, 8 );
		next = Table[next] & 0xFF;
		next += (Table[next] >> 48);
	}

	for ( size_t i = 0; i < sizeof(keyStream); ++i )
		printf("%02x", keyStream[i];
	
	fclose(fp);
	return 0;
}
